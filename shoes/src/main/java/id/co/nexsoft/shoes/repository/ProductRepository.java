package id.co.nexsoft.shoes.repository;

import org.springframework.data.repository.CrudRepository;
import java.util.List;

import id.co.nexsoft.shoes.model.Product;

public interface ProductRepository extends CrudRepository<Product, Integer> {
    Product findById(int id);
    List<Product> findAll();
    void deleteById(int id);
}
