package id.co.nexsoft.shoes.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.shoes.model.Product;
import id.co.nexsoft.shoes.repository.ProductRepository;

@RestController
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping("/product")
    public List<Product> getAllData() {
        return productRepository.findAll();
    }

    @GetMapping("/product/{id}")
    public Product getDataById(@PathVariable int id) {
        return productRepository.findById(id);
    }

    @PostMapping("/product")
    @ResponseStatus(HttpStatus.CREATED)
    public Product addData(@RequestBody Product product) {
        return productRepository.save(product);
    }

    @DeleteMapping("/product/{id}")
    public void deleteData(@PathVariable int id) {
        productRepository.deleteById(id);
    }

    @PutMapping("/product/{id}")
    public ResponseEntity<Object> putDataById(
        @PathVariable int id,
        @RequestBody Product product)
    {
        Optional<Product> products
            = Optional.ofNullable(
                productRepository.findById(id));

        if (!products.isPresent())
            return ResponseEntity
                .notFound()
                .build();

        product.setId(id);

        productRepository.save(product);

        return ResponseEntity
            .noContent()
            .build();
    }
}
