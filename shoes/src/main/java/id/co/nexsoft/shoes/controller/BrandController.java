package id.co.nexsoft.shoes.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.shoes.model.Brand;
import id.co.nexsoft.shoes.repository.BrandRepository;

@RestController
public class BrandController {

    @Autowired
    private BrandRepository brandRepository;

    @GetMapping("/brand")
    public List<Brand> getAllData() {
        return brandRepository.findAll();
    }

    @GetMapping("/brand/{id}")
    public Brand getDataById(@PathVariable int id) {
        return brandRepository.findById(id);
    }

    @PostMapping("/brand")
    @ResponseStatus(HttpStatus.CREATED)
    public Brand addData(@RequestBody Brand brand) {
        return brandRepository.save(brand);
    }

    @DeleteMapping("/brand/{id}")
    public void deleteData(@PathVariable int id) {
        brandRepository.deleteById(id);
    }

    @PutMapping("/brand/{id}")
    public ResponseEntity<Object> putDataById(
        @PathVariable int id,
        @RequestBody Brand brand)
    {
        Optional<Brand> brands
            = Optional.ofNullable(
                brandRepository.findById(id));

        if (!brands.isPresent())
            return ResponseEntity
                .notFound()
                .build();

        brand.setId(id);

        brandRepository.save(brand);

        return ResponseEntity
            .noContent()
            .build();
    }
}
