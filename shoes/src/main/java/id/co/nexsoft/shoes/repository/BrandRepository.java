package id.co.nexsoft.shoes.repository;

import org.springframework.data.repository.CrudRepository;
import java.util.List;

import id.co.nexsoft.shoes.model.Brand;

public interface BrandRepository extends CrudRepository<Brand, Integer> {
    Brand findById(int id);
    List<Brand> findAll();
    void deleteById(int id);
}
