package id.co.nexsoft.booklibrary.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.booklibrary.model.Author;

public interface AuthorRepository extends CrudRepository<Author, Integer> {
    Author findById(int id);
    List<Author> findAll();
    void deleteById(int id);
}
