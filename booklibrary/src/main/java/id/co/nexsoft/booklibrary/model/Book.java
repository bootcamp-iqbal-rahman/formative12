package id.co.nexsoft.booklibrary.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.Max;

@Entity
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String title;
    @Max(value = 2023, message = "Batas tahun publikasi melebihi 2023")
    private int publication_year;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private Author author_id;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category_id;

    public Book() {
    }

    public Book(String title, int publication_year, Author author_id, Category category_id) {
        this.title = title;
        this.publication_year = publication_year;
        this.author_id = author_id;
        this.category_id = category_id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getPublication_year() {
        return publication_year;
    }

    public Author getAuthor_id() {
        return author_id;
    }

    public Category getCategory_id() {
        return category_id;
    }
}