package id.co.nexsoft.booklibrary.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.PutMapping;

import id.co.nexsoft.booklibrary.model.Category;
import id.co.nexsoft.booklibrary.repository.CategoryRepository;

@RestController
public class CategoryController {
    
    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping("/category")
    public List<Category> getAllData() {
        return categoryRepository.findAll();
    }

    @GetMapping("/category/{id}")
    public Category getDataById(@PathVariable int id) {
        return categoryRepository.findById(id);
    }

    @PostMapping("/category")
    @ResponseStatus(HttpStatus.CREATED)
    public Category addData(@RequestBody Category category) {
        return categoryRepository.save(category);
    }

    @DeleteMapping("/category/{id}")
    public void deleteData(@PathVariable int id) {
        categoryRepository.deleteById(id);
    }

    @PutMapping("/category/{id}")
    public ResponseEntity<Object> putDataById(
        @PathVariable int id, 
        @RequestBody Category category) 
    {
        Optional<Category> categorys
            = Optional.ofNullable(
                categoryRepository.findById(id));

        if (!categorys.isPresent())
            return ResponseEntity
                .notFound()
                .build();

        category.setId(id);

        categoryRepository.save(category);

        return ResponseEntity
            .noContent()
            .build();
    }
}
