package id.co.nexsoft.booklibrary.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.PutMapping;

import id.co.nexsoft.booklibrary.model.Author;
import id.co.nexsoft.booklibrary.repository.AuthorRepository;

@RestController
public class AuthorController {
    
    @Autowired
    private AuthorRepository authorRepository;

    @GetMapping("/author")
    public List<Author> getAllData() {
        return authorRepository.findAll();
    }

    @GetMapping("/author/{id}")
    public Author getDataById(@PathVariable int id) {
        return authorRepository.findById(id);
    }

    @PostMapping("/author")
    @ResponseStatus(HttpStatus.CREATED)
    public Author addData(@RequestBody Author author) {
        return authorRepository.save(author);
    }

    @DeleteMapping("/author/{id}")
    public void deleteData(@PathVariable int id) {
        authorRepository.deleteById(id);
    }

    @PutMapping("/author/{id}")
    public ResponseEntity<Object> putDataById(
        @PathVariable int id, 
        @RequestBody Author author) 
    {
        Optional<Author> authors
            = Optional.ofNullable(
                authorRepository.findById(id));

        if (!authors.isPresent())
            return ResponseEntity
                .notFound()
                .build();

        author.setId(id);

        authorRepository.save(author);

        return ResponseEntity
            .noContent()
            .build();
    }
}
