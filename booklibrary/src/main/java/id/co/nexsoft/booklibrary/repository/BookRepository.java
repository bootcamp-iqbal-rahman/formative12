package id.co.nexsoft.booklibrary.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.booklibrary.model.Book;

public interface BookRepository extends CrudRepository<Book, Integer> {
    Book findById(int id);
    List<Book> findAll();
    void deleteById(int id);
}
