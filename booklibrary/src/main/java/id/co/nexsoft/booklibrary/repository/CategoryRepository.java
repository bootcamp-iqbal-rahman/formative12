package id.co.nexsoft.booklibrary.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.booklibrary.model.Category;

public interface CategoryRepository extends CrudRepository<Category, Integer> {
    Category findById(int id);
    List<Category> findAll();
    void deleteById(int id);
}
