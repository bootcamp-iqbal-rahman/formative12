package id.co.nexsoft.booklibrary.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.PutMapping;

import id.co.nexsoft.booklibrary.model.Book;
import id.co.nexsoft.booklibrary.repository.BookRepository;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;

@RestController
public class BookController {
    
    @Autowired
    private BookRepository bookRepository;

    @GetMapping("/book")
    public List<Book> getAllData() {
        return bookRepository.findAll();
    }

    @GetMapping("/book/{id}")
    public Book getDataById(@PathVariable int id) {
        return bookRepository.findById(id);
    }

    @PostMapping("/book")
    @ResponseStatus(HttpStatus.CREATED)
    @NotBlank(message = "Data ada yang koskong")
    public Book addData(@RequestBody @Valid Book Book) {
        return bookRepository.save(Book);
    }

    @DeleteMapping("/book/{id}")
    public void deleteData(@PathVariable int id) {
        bookRepository.deleteById(id);
    }

    @PutMapping("/book/{id}")
    public ResponseEntity<Object> putDataById(
        @PathVariable int id, 
        @RequestBody Book book) 
    {
        Optional<Book> Books
            = Optional.ofNullable(
                bookRepository.findById(id));

        if (!Books.isPresent())
            return ResponseEntity
                .notFound()
                .build();

        book.setId(id);

        bookRepository.save(book);

        return ResponseEntity
            .noContent()
            .build();
    }
}
