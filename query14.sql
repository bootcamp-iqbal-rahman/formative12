CREATE table brand (
  	id int AUTOINCREMENT,
  	name varchar(255),
  	PRIMARY KEY (id)
);
  
CREATE TABLE product (
  	id int AUTOINCREMENT,
  	artnumber varchar(50),
  	name varchar(50),
  	description varchar(150),
  	brand_id int,
  	FOREIGN KEY (brand_id) REFERENCES brand(id)	
);

insert into brand 
	(name)
VALUES
	('Nike'), ('Adidas'), ('Bata')
;

insert into product
	(artnumber, name, description, brand_id)
VALUES
	('CV8121-100', 'Jordan Delta 2', 'some desc', 1),
    ('CT8527-400', 'Air Jordan 4', 'some desc', 1)
;